// JavaScript Document
$(document).ready(function() {
    'use strict';

    /************************************************************************************ PARALLAX STARTS */

    $('.parallax-1').parallax("50%", 0.02);
    $('.parallax-map').parallax("50%", 0.5);
    $('.parallax-2').parallax("50%", 0.8);
    $('.parallax-6').parallax("50%", 0.8);
    $('.parallax-7').parallax("50%", 0.02);
    $('.parallax-8').parallax("50%", 0.01);
    $('.parallax-inner.page-01').parallax("50%", 1.2);
    $('.parallax-inner.page-02').parallax("50%", 1.2);
    $('.parallax-inner.page-03').parallax("50%", 1.2);
    $('.parallax-inner.page-04').parallax("50%", 1.2);
    $('.parallax-inner.page-05').parallax("50%", 1.2);
    $('.parallax-inner.page-06').parallax("50%", 1.2);
    $('.parallax-inner.page-07').parallax("50%", 1.2);
    $('.parallax-inner.page-08').parallax("50%", 1.2);
    $('.parallax-inner.page-09').parallax("50%", 1.2);
    $('.parallax-inner.page-10').parallax("50%", 1.2);
    $('.parallax-inner.page-11').parallax("50%", 1.2);
    $('.parallax-inner.page-12').parallax("50%", 1.2);
	$('.parallax-inner.page-13').parallax("50%", 1.2);
	$('.parallax-inner.page-14').parallax("50%", 1.2);
	$('.parallax-inner.page-15').parallax("50%", 1.2);
	$('.parallax-inner.page-16').parallax("50%", 1.2);
	$('.parallax-inner.page-17').parallax("50%", 1.2);
	$('.parallax-inner.page-18').parallax("50%", 1.2);
	$('.parallax-inner.page-19').parallax("50%", 1.2);
    $('.parallax-inner.demo-page').parallax("50%", 1.5);

    /************************************************************************************ PARALLAX ENDS */

    /************************************************************************************ STICKY NAVIGATION STARTS */

    $("#navigation").sticky({
        topSpacing: 0
    });

    /************************************************************************************ STICKY NAVIGATION ENDS */

    /************************************************************************************ DROPDOWN HOVER MENU STARTS */
	
	$('.js-activated').dropdownHover().dropdown();

    /************************************************************************************ DROPDOWN HOVER MENU ENDS */

    /************************************************************************************ ONEPAGE NAVIGATION STARTS */

    $('.nav').onePageNav({
        filter: ':not(.external)',
        begin: function() {
            console.log('start')
        },
        end: function() {
            console.log('stop')
        }
    });

    /************************************************************************************ ONEPAGE NAVIGATION ENDS */

    /************************************************************************************ PAGE ANIMATED ITEMS STARTS */

	$('.animated').appear(function() {
		var elem = $(this);
		var animation = elem.data('animation');
		if (!elem.hasClass('visible')) {
			var animationDelay = elem.data('animation-delay');
			if (animationDelay) {

				setTimeout(function() {
					elem.addClass(animation + " visible");
				}, animationDelay);

			} else {
				elem.addClass(animation + " visible");
			}
		}
	});

    /************************************************************************************ PAGE ANIMATED ITEMS ENDS */

    /************************************************************************************ WE BUILD CAROUSEL STARTS */

    //Sort random function

    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".we-build-carousel").owlCarousel({
        autoPlay: false,
        slideSpeed: 500,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
        autoHeight: true,
        pagination: true,
        navigation: false,
        transitionStyle: "fade",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    /************************************************************************************ WE BUILD CAROUSEL ENDS */

    /************************************************************************************ PROJECTS CAROUSEL STARTS */

    //Sort random function

    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".projects-carousel").owlCarousel({
        autoPlay: 5000,
        slideSpeed: 500,
        items: 4,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 2],
        itemsMobile: [479, 1],
        autoHeight: true,
        pagination: true,
        navigation: false,
        transitionStyle: "fade",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    /************************************************************************************ PROJECTS CAROUSEL ENDS */

    /************************************************************************************ PICTURES CAROUSEL STARTS */

    //Sort random function

    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".pictures-carousel").owlCarousel({
        autoPlay: 5000,
        slideSpeed: 500,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
        autoHeight: true,
        pagination: false,
        navigation: true,
        transitionStyle: "fade",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    /************************************************************************************ PICTURES CAROUSEL ENDS */

    /************************************************************************************ OUR CHEFS CAROUSEL STARTS */

    //Sort random function

    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".our-chefs-carousel").owlCarousel({
        autoPlay: false,
        slideSpeed: 500,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
        autoHeight: true,
        pagination: false,
        navigation: true,
        transitionStyle: "fade",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    /************************************************************************************ OUR CHEFS CAROUSEL ENDS */

    /************************************************************************************ GALLERY CAROUSEL STARTS */

    //Sort random function

    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".gallery-carousel").owlCarousel({
        autoPlay: false,
        slideSpeed: 500,
        items: 6,
        itemsDesktop: [1199, 6],
        itemsDesktopSmall: [979, 4],
        itemsTablet: [768, 3],
        itemsMobile: [479, 2],
        autoHeight: true,
        pagination: true,
        navigation: false,
        transitionStyle: "fade",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    /************************************************************************************ GALLERY CAROUSEL ENDS */

    /************************************************************************************ OUR SELECTION CAROUSEL STARTS */

    //Sort random function

    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".our-selection-carousel").owlCarousel({
        autoPlay: false,
        slideSpeed: 500,
        items: 4,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 2],
        itemsMobile: [479, 1],
        autoHeight: true,
        pagination: true,
        navigation: false,
        transitionStyle: "fade",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    /************************************************************************************ OUR SELECTION CAROUSEL ENDS */

    /************************************************************************************ BLOG CAROUSEL STARTS */

    //Sort random function

    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".blog-carousel").owlCarousel({
        autoPlay: false,
        slideSpeed: 500,
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 2],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
        autoHeight: true,
        pagination: false,
        navigation: true,
        transitionStyle: "fade",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    /************************************************************************************ BLOG CAROUSEL ENDS */

    /************************************************************************************ TESTIMONIALS CAROUSEL STARTS */

    //Sort random function

    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".testimonial-carousel").owlCarousel({
        autoPlay: 5000,
        slideSpeed: 500,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
        autoHeight: true,
        pagination: true,
        navigation: false,
        transitionStyle: "fade",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    /************************************************************************************ TESTIMONIALS CAROUSEL ENDS */

    /************************************************************************************ FITVID STARTS */

    $(".fitvid").fitVids();

    /************************************************************************************ FITVID ENDS */

    /************************************************************************************ TO TOP STARTS */

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').on("click", function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    /************************************************************************************ TO TOP ENDS */

    /************************************************************************************ MAGNIFIC POPUP STARTS */

    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        /*disableOn: 700,*/
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

    $('.image-popup-vertical-fit').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
            verticalFit: true
        }

    });

    $('.image-popup-fit-width').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: {
            verticalFit: false
        }
    });

    $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

    $('.simple-ajax-popup-align-top').magnificPopup({
        type: 'ajax',
        alignTop: true,
        overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
    });

    $('.simple-ajax-popup').magnificPopup({
        type: 'ajax'
    });


    $(document).ready(function() {
        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#name',

            // When elemened is focused, some mobile browsers in some cases zoom in
            // It looks not nice, so we disable it:
            callbacks: {
                beforeOpen: function() {
                    if ($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                }
            }
        });
    });


    /************************************************************************************ MAGNIFIC POPUP ENDS */

    /************************************************************************************ PRELOADER STARTS */

    jQuery(window).load(function() {

        $('#preloader').fadeOut('slow');

    });

    /************************************************************************************ PRELOADER ENDS */

    /************************************************************************************ MAP STARTS */

    
	(function($) {
        "use strict"; // Start of use strict

        $(document).ready(function() {

            $(window).trigger("resize");

            init_google_map();

        });

    })(jQuery); // End of use strict


    /* ---------------------------------------------
     Google map
     --------------------------------------------- */

    var gmMapDiv = $("#map-canvas");

    function init_google_map() {
        (function($) {

            // Open/Close map        
            $("#see-map").click(function() {
                $(this).toggleClass("js-active");

                if ($("html").hasClass("mobile")) {
                    gmMapDiv.hide();
                    gmMapDiv.gmap3({
                        action: "destroy"
                    }).empty().remove();
                } else {
                    gmMapDiv.slideUp(function() {
                        gmMapDiv.gmap3({
                            action: "destroy"
                        }).empty().remove();
                    })
                }

                gmMapDiv.slideToggle(400, function() {

                    if ($("#see-map").hasClass("js-active")) {
                        $(".google-map").append(gmMapDiv);
                        init_map();
                    }

                });

                setTimeout(function() {
                    $("html, body").animate({
                        scrollTop: $("#see-map").offset().top
                    }, "slow", "easeInBack");
                }, 100);


                return false;
            });
        })(jQuery);
    }


    function init_map() {
        (function($) {
            if (gmMapDiv.length) {

                var gmCenterAddress = gmMapDiv.attr("data-address");
                var gmMarkerAddress = gmMapDiv.attr("data-address");
                var gmColor = gmMapDiv.attr("data-color");


                gmMapDiv.gmap3({
                    action: "init",
                    marker: {
                        address: gmMarkerAddress,
                        options: {
                            icon: "images/icons/map-marker.png"
                        }
                    },
                    map: {
                        options: {
                            styles: [{
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": gmColor
                                }, {
                                    "lightness": 17
                                }]
                            }, {
                                "featureType": "landscape",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": gmColor
                                }, {
                                    "lightness": 20
                                }]
                            }, {
                                "featureType": "road.highway",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": gmColor
                                }, {
                                    "lightness": 17
                                }]
                            }, {
                                "featureType": "road.highway",
                                "elementType": "geometry.stroke",
                                "stylers": [{
                                    "color": gmColor
                                }, {
                                    "lightness": 29
                                }, {
                                    "weight": 0.2
                                }]
                            }, {
                                "featureType": "road.arterial",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": gmColor
                                }, {
                                    "lightness": 18
                                }]
                            }, {
                                "featureType": "road.local",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": gmColor
                                }, {
                                    "lightness": 16
                                }]
                            }, {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": gmColor
                                }, {
                                    "lightness": 21
                                }]
                            }, {
                                "elementType": "labels.text.stroke",
                                "stylers": [{
                                    "visibility": "on"
                                }, {
                                    "color": gmColor
                                }, {
                                    "lightness": 16
                                }]
                            }, {
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "saturation": 36
                                }, {
                                    "color": gmColor
                                }, {
                                    "lightness": 40
                                }]
                            }, {
                                "elementType": "labels.icon",
                                "stylers": [{
                                    "visibility": "off"
                                }]
                            }, {
                                "featureType": "transit",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": gmColor
                                }, {
                                    "lightness": 19
                                }]
                            }, {
                                "featureType": "administrative",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": gmColor
                                }, {
                                    "lightness": 20
                                }]
                            }, {
                                "featureType": "administrative",
                                "elementType": "geometry.stroke",
                                "stylers": [{
                                    "color": gmColor
                                }, {
                                    "lightness": 17
                                }, {
                                    "weight": 1.2
                                }]
                            }, ],
                            zoom: 14,
                            zoomControl: true,
                            zoomControlOptions: {
                                style: google.maps.ZoomControlStyle.SMALL
                            },
                            mapTypeControl: false,
                            scaleControl: false,
                            scrollwheel: false,
                            streetViewControl: false,
                            draggable: true
                        }
                    }
                });
            }
        })(jQuery);
    }


    /************************************************************************************ MAP ENDS */

    /************************************************************************************ COMING SOON PAGE COUNTER STARTS */

    (function($) {
        "use strict"; // Start of use strict

        var finalDate = '2016/01/01';

        $('div#counter').countdown(finalDate)
            .on('update.countdown', function(event) {

                $(this).html(event.strftime('<span>%D <em>days</em></span>' +
                    '<span>%H <em>hours</em></span>' +
                    '<span>%M <em>minutes</em></span>' +
                    '<span>%S <em>seconds</em></span>'));

            });

    });


    /************************************************************************************ COMING SOON PAGE COUNTER ENDS */

	}); //$(document).ready(function(){

	/************************************************************************************ OVERLAY NAVIGATION ENDS */

	function init() {
		window.addEventListener('scroll', function(e) {
			var distanceY = window.pageYOffset || document.documentElement.scrollTop,
				shrinkOn = 90,
				nav = document.querySelector("nav");
			tc = document.querySelector("div#tc");
			if (distanceY > shrinkOn) {
				classie.add(nav, "smaller");
			} else {
				if (classie.has(nav, "smaller")) {
					classie.remove(nav, "smaller");
				}
			}
			if (distanceY > shrinkOn) {
				classie.add(tc, "hide");
			} else {
				if (classie.has(tc, "hide")) {
					classie.remove(tc, "hide");
				}
			}
		});
	}
	window.onload = init();


	/************************************************************************************ OVERLAY NAVIGATION ENDS */